## CI CD Extension Example

This project functions as an example of calling the CI extension [Git Diff Revision Activity Metrics](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/git-diff-revision-activity-metrics#iteration-heritage).

This extensions work can be seen in the Merge Requests and the job logs for the MRs.
